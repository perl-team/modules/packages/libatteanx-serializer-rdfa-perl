libatteanx-serializer-rdfa-perl (0.110-4) unstable; urgency=medium

  * stop avoid Test::Modern (not superfluous after all)
  * tolerate Test::Modern warnings

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 02 Mar 2025 14:29:25 +0100

libatteanx-serializer-rdfa-perl (0.110-3) unstable; urgency=medium

  * add patch 1001 to avoid superfluous use of Test::Modern;
    closes: bug#1093439, thanks to Santiago Vila
  * declare compliance with Debian Policy 4.7.2
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 02 Mar 2025 13:16:36 +0100

libatteanx-serializer-rdfa-perl (0.110-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 20:31:23 +0000

libatteanx-serializer-rdfa-perl (0.110-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * drop patch 1001 adopted upstream
  * copyright: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 17 Feb 2021 12:59:50 +0100

libatteanx-serializer-rdfa-perl (0.100-2) unstable; urgency=medium

  [ Debian Janitor ]
  * build-depend on debhelper-compat (not debhelper)
  * set upstream metadata fields:
    Bug-Database Bug-Submit Repository Repository-Browse

  [ Jonas Smedegaard ]
  * use debhelper compatibility level 13 (not 9)
  * declare compliance with Debian Policy 4.5.1
  * relax to build-depend unversioned
    on libattean-perl librdf-rdfa-generator-perl:
    needed version satisfied in all supported releases of Debian
  * add patch to fix declare file_extensions needed by recent Attean;
    closes: bug#982722, thanks to Lucas Nussbaum
  * copyright:
    + update coverage
    + drop duplicate source URL

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 13 Feb 2021 20:07:48 +0100

libatteanx-serializer-rdfa-perl (0.100-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 17:18:59 +0100

libatteanx-serializer-rdfa-perl (0.100-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Extend coverage for main upstream author.
    + Extend coverage of packaging.
  * Update watch file: Rewrite usage comment.
  * Mark build-dependencies needed only for testsuite as such.
  * Declare compliance with Debian Policy 4.3.0.
  * Build-depend on librdf-prefixes-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 22 Feb 2019 02:40:12 +0100

libatteanx-serializer-rdfa-perl (0.01-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#890241.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Nov 2018 13:03:48 +0100
